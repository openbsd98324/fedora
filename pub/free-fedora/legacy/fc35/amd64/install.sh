

echo === THIS WILL WIPE /DEV/SDA === 
echo === PRESS ENTER TO INSTALL MBR/LEGACY FREE-FEDORA to /target === 
read 

echo -- Note tmp target is selected in order to have fs permissions --
mkdir /tmp/target


echo INSTALL MBR 
echo ===========
wget -c --no-check-certificate     'https://gitlab.com/openbsd98324/fedora/-/raw/main/pub/free-fedora/legacy/fc35/amd64/image-mbr-4parts.img.gz'  -O mbr.img.gz  
zcat mbr.img.gz > /dev/sda 



echo INSTALL FC35 FREE FEDORA 
echo ========================
wget -c --no-check-certificate    'https://gitlab.com/openbsd98324/fedora/-/raw/main/pub/free-fedora/legacy/fc35/amd64/fedora-fc35-x86_64-legacy-rootfs-mnt-sysroot-v1.2.tar.gz'  -O  freefedora.tar.gz 
mkfs.ext3 -F /dev/sda4

sync
mount /dev/sda4 /tmp/target 
mount
mount /dev/sda4 /tmp/target 
sync
cp freefedora.tar.gz /tmp/target/ 


cd /tmp/target
tar xvpfz freefedora.tar.gz 


echo '/dev/sda4   /               ext4    defaults,noatime  0       1' > /tmp/target/etc/fstab


sync


echo == The Free Fedora, OpenSource, more FOSS is now installed == 

